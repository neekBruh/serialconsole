#ifndef SCANCODES_H
#define SCANCODES_H

#define UP_ARROW 0
#define DOWN_ARROW 1
#define RIGHT_ARROW 2
#define LEFT_ARROW 3

namespace ScanCodes {

	const char uppArrow[][4] = {{0x1B, 0x5B, 0x41, 0x00}, //up	
	                             {0x1B, 0x5B, 0x42, 0x00}, //down
	                             {0x1B, 0x5B, 0x43, 0x00}, //right
	                             {0x1B, 0x5B, 0x44, 0x00} //left
	                            };

}

#endif
