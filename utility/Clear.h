#ifndef CLEAR_H
#define CLEAR_H

namespace clear {

	void clear(void) {
		
	    Serial.write(27);       // ESC command
	    Serial.print(F("[2J"));    // clear screen command
	    Serial.write(27);
	    Serial.print(F("[H"));     // cursor to home command
	}
}

#endif
