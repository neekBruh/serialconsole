/*
	This file was created with the purpose of 
	stating every possible error string to be printed
	in the command line.
	
*/

#ifndef ERRORS_H
#define ERRORS_H 

#include <avr/pgmspace.h>

#define SYNTAX_ERROR -1
#define BUFFER_ERROR -2
#define CMD_NOT_FOUND -3
#define FLAG_NOT_FOUND -4
#define MISSING_TERMINATION -5

namespace Errors {
	
	const char err_0[] PROGMEM = "Error: syntax is incorrenct";
	const char err_1[] PROGMEM = "Error: buffer size is unable to hold every command statement";
	const char err_2[] PROGMEM = "Error: command not found";
	const char err_3[] PROGMEM = "Error: flag not found";
	const char err_4[] PROGMEM = "Error: missing CR or NL";

	const char* const err_table[] PROGMEM  = {err_0, err_1, err_2, err_3, err_4};
}

#endif
