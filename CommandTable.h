/*
	In this file every command is declared. 
	To add your own, just copy and paste following the syntax.
	Currently, up to 256 commands are supported
	
*/

#ifndef COMMANDTABLE_H
#define COMMANDTABLE_H

#include <Arduino.h>
#include <avr/pgmspace.h>



namespace CommandTable {
	
//main commands
const char cmd_0[] PROGMEM = "clear";
const char cmd_1[] PROGMEM = "date";
const char cmd_2[] PROGMEM = "exit";
//...
//...
//const char cmd_x[] PROGMEM = "my_custom_command";


const char* const cmd_table[] PROGMEM  = {cmd_0, cmd_1, cmd_2};
//const char* const cmd_table[] PROGMEM  = {cmd_0, cmd_1, cmd_2, ... , cmd_x};
	
	
}
#endif
