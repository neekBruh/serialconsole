#ifndef ARDUINOSERIALCONSOLE_H
#define ARDUINOSERIALCONSOLE_H

//#define DEBUG

#include <Arduino.h>
#include <string.h>
#include "CommandTable.h"
#include "Errors.h"
#include "General.h"
#include "utility/ScanCodes.h"
//Notes:
//data[]: It's used to store data comming from the serial port for the purpose of processing them afterwards.
//		  It's also used as a buffer to print errors in the serial port

class SerialConsole {
	
private:
	//private vars
	static char data[BUFFER_SIZE]; //definition of the data char array
	int8_t status;
	
	int8_t buffer_pos;
	int8_t current_buffer_length;
	bool execute;
	uint32_t timeout;
	int8_t scan_cnt;
	char c;
	char buff[6]; 
	
	functionPointer* functions; //pointer to array of function pointers
	uint8_t num_of_fun;			//number of functions attached
	char **keywords;	//double pointer to array of function keywords that the console will look for
	bool functAttached;

	//private methods
	int8_t parseString(char input[], const uint16_t input_size, char** output, uint8_t rows);

	bool checkDoubleQcnt(char input[], uint8_t size);
	bool checkDoubleQSpacing(char input[], uint8_t size);
	char* findFirstChar(char* input, uint8_t size);

	bool checkTermination(char* input);
	
	int8_t findAndExecute(void);	
	void printError(int8_t err);
	
	//console manipulation
	void shiftRight(char input[], int8_t input_size);
	void shiftLeft(char input[], int8_t input_size);
	void backCursor(int8_t x);
	void turnOffCursor(void);
	void turnOnCursor(void);
	
	//auxiliar
	int8_t digitCount(int8_t num);


public: 
	//constructor/s and destructors
	SerialConsole(void);
	~SerialConsole(void);
	
	//public methods
	void init(const int32_t &_baud);
	void readSerial(void);
	void showPrompt(void) const;
	
	void attachFunctions(functionPointer functions[], char** keywords, int num_of_fun);
	
	#ifdef DEBUG
	//DEBUG
	void printArray(const char *input, const uint8_t rows, const uint8_t columns);
	void printChars(const char *input, const uint8_t rows, const uint8_t columns);
	void printStringArray(const char **input, const uint8_t size);
	#endif

	
	//public vars
	//definition of static vars
	static char** argv;
	static int8_t argc;

};

#endif
