# Basic Arduino Serial Console

This is a basic unix-like serial console for Arduino. 

## Features
*	Parses individual strings received in the Serial port.
*	Extra functionality can be added to respond to different commands. It has a command table where extra commands can be added. 
*	Supports Basic Syntax checking.

## Public Methods
`init(baud):` Initializes the serial port at the specified baud and clears the internal buffers.  
`readSerial():` Reads the serial port and calls the core functions of the console.  
`showPrompt():` Prints a visual queue to the user that the console is ready to receive commands.  

## Public variables
`argv[32][16]:` Holds the arguments parsed by the console. It is "similar" to the standard main argv in c/c++.  
`argc:` Holds the amount of arguments parsed. Again, it's similar to the standard main argc in c/c++.  

## How to use
Use any serial monitor like PuTTY(Recommended) or the Serial Monitor in the Arduino IDE.

NOTE: If you are using PuTTY, force on Local echo and Local Line editing (Remote line editing is not supported yet).

## Author
* **Nicolas** - [neekBruh](https://gitlab.com/neekBruh)