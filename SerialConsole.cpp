#include "SerialConsole.h"

//declaration of static vars
//can be accesed anywhere by any object outside the class
char SerialConsole::data[BUFFER_SIZE];
char** SerialConsole::argv;
int8_t SerialConsole::argc;

//Constructors
SerialConsole::SerialConsole(void) {
	
	buffer_pos = 0;
	execute = false;
	timeout = 0;
	scan_cnt = 0;
	current_buffer_length = 0;
	
	//TODO:
	//Check if memory is correctly allocated afterwards
	
	//allocate buffers
	
	//allocate rows and set them to NULL
	argv = new char*[ARGV_ROWS];
	for(int i = 0; i < ARGV_ROWS; ++i)
		argv[i] = '\0';

	
	//clear buffers
	memset(data, '\0', BUFFER_SIZE);
	functAttached = false;
}

SerialConsole::~SerialConsole(void) {
	
	//deallocate rows
	delete[] argv;
	
	//clear buffers
	memset(data, '\0', BUFFER_SIZE);
}

//////////////////////////public methods//////////////////////////

void SerialConsole::init(const int32_t &_baud) {
	uint8_t usart_status;
	
	//Read status of USART transmitter
	usart_status = (UCSR0B & (1<<TXEN0))>>TXEN0;
	
	if(usart_status == 0) {
		Serial.begin(_baud);
		//Serial.setTimeout(DEFAULT_CUSTOM_TIMEOUT);
	}
	
}

void SerialConsole::readSerial(void) {
	
	//read from serial buffer
	while (Serial.available()) {
		c = Serial.read();
		
		switch(c) {
			
			//if ESC(0x1B) is found, it is the start of a scan code and should be treated differently
			case 0x1B: {
				buff[scan_cnt] = c;
				timeout = millis();
				
				while(millis() - timeout < DEFAULT_TIMEOUT) {
				
					while (Serial.available()) {
					
						if(scan_cnt < 4) ++scan_cnt;
						else break;
						
						
						c = Serial.read();
						buff[scan_cnt] = c;
						
						if(c == 0x7E) break;
					}
				
					if(c == 0x7E) break;
				}
				
				if(scan_cnt >=2) { 
			        if(strncmp(ScanCodes::uppArrow[LEFT_ARROW] ,buff, 3) == 0 and buffer_pos > 0 ) {
			        	--buffer_pos;
			        	Serial.write(buff, 3);
					}
			        else if(strncmp(ScanCodes::uppArrow[RIGHT_ARROW] ,buff, 3) == 0 and buffer_pos < current_buffer_length) {
			        	++buffer_pos;
			        	Serial.write(buff, 3);
					}
			    }
        
				scan_cnt = 0;
				break;
			}
			
			//Simple ENTER
			case '\r': case '\n': {
				Serial.println();
				
				if(current_buffer_length > 0 and buffer_pos < BUFFER_SIZE-1) {

					data[current_buffer_length] = c;
					
					execute = true;
					buffer_pos = 0;
				} 
				else showPrompt();

				break;
			}
			
			//BACKSPACE
			case 0x7F: {
				if(buffer_pos > 0 and current_buffer_length > 0) {
					
					Serial.write(c);
					
					if(current_buffer_length != buffer_pos and buffer_pos >= 1) {
						shiftLeft(&data[buffer_pos], current_buffer_length - buffer_pos);
						
						turnOffCursor();
						Serial.write(&data[buffer_pos-1], current_buffer_length - buffer_pos+1);
						backCursor(current_buffer_length - buffer_pos+1);
						turnOnCursor();
					}
					
					--buffer_pos;
					--current_buffer_length;
					
				}
				break;
			}
			
			//Everything else
			default: {
				if(c > 31 and c < 128 and current_buffer_length < BUFFER_SIZE-2) {//prevents chars like � and � from being printed and buffer overflow
					
					if(current_buffer_length != buffer_pos and current_buffer_length-1 < BUFFER_SIZE-2) {
						//shift the characters between the cursor and the end, and then print them
						shiftRight(&data[buffer_pos], current_buffer_length - buffer_pos);
						
						turnOffCursor();
						Serial.write(&data[buffer_pos], current_buffer_length + 1 - buffer_pos);
						
						//return the cursor to the original position
						backCursor(current_buffer_length + 1 - buffer_pos);
						turnOnCursor();
					}
					
					data[buffer_pos] = c;
					++buffer_pos;
					++current_buffer_length;
					Serial.write(c);
				}  
				break;
			}
		}
	}
	
	if(execute) {

		//parse arguments
		if(checkTermination(data)) {//looks for \r or \n chars
			
			if(checkDoubleQcnt(data, current_buffer_length+1) and checkDoubleQSpacing(data, current_buffer_length+1)) {
				status = parseString(data, current_buffer_length+1, argv, ARGV_ROWS);
			} else status = SYNTAX_ERROR;	
			
		} else status = MISSING_TERMINATION;
		
		//Error printing
		if(status < 0) {
			//display error in serial
			printError(status);
				
		} else {//read ok
			//save argument count in argc
			argc = status;
			
			//search first command in list
			#ifndef DEBUG
			status = findAndExecute();
			#endif
			#ifdef DEBUG
			//printChars(&argv[0][0], argc, ARGV_ROWS); //ARGVROWS isnt necessary anymore
			printStringArray(argv, argc);
			#endif DEBUG
	
			if(status < 0) {
				//display error in serial
				printError(status);
			}
		}
				
		//clear buffers
		execute = false;
		current_buffer_length = 0;
		showPrompt();
	}
}

void SerialConsole::showPrompt(void) const {
	Serial.print(F("Arduino>"));
}

/***************************************************************
 					attachFunctions() function
	This function will receive an array of function pointers and
	will point the class functionPointer to the array passed.
	It is assumed that every function has one and only one keyword
	associated with it.

**************************************************************/

void SerialConsole::attachFunctions(functionPointer functions[], char** keywords, int num_of_fun) {
		functAttached = true;
		this->functions = functions;
		this->keywords = keywords;
		this->num_of_fun = num_of_fun;
}


//////////////////////////private methods//////////////////////////


/***************************************************************
 					findAndExecute() function
	This function will try to find the command in the command table.
	If successful, it will execute it. If not, it will return an
	error number.

**************************************************************/

int8_t SerialConsole::findAndExecute(void) {
	
	//compute the command table size
	const uint8_t table_size = sizeof(CommandTable::cmd_table) / sizeof(CommandTable::cmd_table[0]);

	//it's designed to take the data strings of size BUFFER_SIZE
	uint8_t i;
	for(i = 0; i < table_size; ++i) 
		if(strcmp_P(&argv[0][0], (char *)pgm_read_word(&CommandTable::cmd_table[i])) == 0) break;	
	
	if(i < table_size) { //if any command is found in flash command table
		switch(i) {
			
			case 0: {//clear
				Serial.println(F("clear"));
				//do something
				//....
				return 0;

			} 
			case 1: {//date
				Serial.println(F("date"));
				//do something
				//....
				return 0;
			}
			case 2: {//exit
				Serial.println(F("exit"));
				//do something
				//....
				return 0;
			}
		}		
	}
	
	//only search in SRAM if external functions exist
	if(functAttached) {
		//search for functions attached in SRAM
		for(i = 0; i < num_of_fun; ++i) 
			if(strcmp(&argv[0][0], keywords[i]) == 0) break;
		
		//execute the function if found
		if(i < num_of_fun) {
			(*functions[i])();	
			return 0;
		}	
	}
	
	//if no other return case is met, then exit with error code
	return CMD_NOT_FOUND;

}

/***************************************************************
 				Syntax and termination char checkers

***************************************************************/

bool SerialConsole::checkDoubleQcnt(char input[], uint8_t size) {

	uint8_t quote_cnt = 0;
	char* ptr = strchr(input, '\"');

	while(ptr != nullptr and (ptr+1 - input) < size) {
		ptr = strchr(ptr+1, '\"');
		++quote_cnt;
	}

	if(quote_cnt%2 == 0) return true;
	else return false;

}

/***************************************************************
	checkDoubleQSpacing() function
	//return error if:
	//- Is an opening quote and the previous character is not a space
	//- Is a closing quote and the next character is not a space and is not at the end of the string (..asd"\n is allowed)

***************************************************************/

bool SerialConsole::checkDoubleQSpacing(char input[], uint8_t size) {
	char* quote, *starter_char;
	bool opener_quote = true;

	//locate first char of the whole string
	starter_char = findFirstChar(input, size);

	//locate the nearest quote
	quote = (char*)memchr(starter_char, '"', size);

	while(quote != nullptr) {

		//if it is an opener quote and is not the first char in the string, check if it has a space beforehand.
		if(opener_quote) {
			if(quote != starter_char)
				if(quote[-1] != ' ') return false;
		}
		//if is not an opener quote and is not the last char before the NL or CR, check if it has a space afterwards
		else
			if(quote[1] != input[size-1])
				if(quote[1] != ' ') return false;

		opener_quote = !opener_quote;
		++quote;
		quote = (char*)memchr(quote, '"', input+size-quote);


	}

	//here, either there is no quote in the entire string, or
	//no return false condition was met before, indicating a good syntax
	return true;

}


bool SerialConsole::checkTermination(char* input) {
	if(strchr(input, '\r') != nullptr or strchr(input, '\n') != nullptr) return true;
	else return false;
}


/***************************************************************

					findFirstChar() function
				Locates the start of the string

***************************************************************/

char* SerialConsole::findFirstChar(char* input, uint8_t size) {

	uint8_t i;
	for(i = 0; i < size; ++i)
		if(input[i] != ' ') break;

	if(i == size) return nullptr;
	else return input+i;
}

/***************************************************************
 					parseString() function
  Parses every command separated with a space or between double quotes, UNIX like.
  This function extracts the words as much as the output dimensions can handle. So
  it is posible to only extract the first words of the input buffer by changing the rows
  parameter.

***************************************************************/

int8_t SerialConsole::parseString(char input[], const uint16_t input_size, char** output, uint8_t rows) {

	uint8_t argc = 0;

	char* separator_ptr, *starter_char;
	char separators[] = {' ', '\"'};
	uint8_t separator_selector = 0;

	//search first char of the whole string
	starter_char = findFirstChar(input, input_size);

	//check if it is a "
	if(starter_char != nullptr) {
		if(*starter_char == '\"') {
			separator_selector = 1;
			++starter_char;
		}
	} else return SYNTAX_ERROR;

	while(starter_char - input < input_size) {

		//save parameter to output and increase counter
		output[argc] = starter_char;
		++argc;

		//locate nearest separator
		separator_ptr = (char*)memchr(starter_char, separators[separator_selector], input+input_size-starter_char);

		if(separator_ptr != nullptr) {
			//set space to NULL
			*separator_ptr = '\0';

			//skip this NULL
			starter_char = separator_ptr+1;

			//locate next first char
			starter_char = findFirstChar(starter_char, input+input_size-starter_char);

			//check if the first character is a "
			if(*starter_char == '\"') {
				separator_selector = 1;
				++starter_char;
			} else if(*starter_char == '\n' or *starter_char == '\r') return argc;
			  else separator_selector = 0;
		}
		//the CR and/or the LN are right next to the last parameter (hence no separator next)
		//locate the nearest one and set it to NULL
		else {
			separator_ptr = (char*)memchr(starter_char, '\n', input+input_size-starter_char);

			if(separator_ptr != nullptr) *separator_ptr = '\0';
			else {
				separator_ptr = (char*)memchr(starter_char, '\r', input+input_size-starter_char);
				*separator_ptr = '\0';
			}

			break;
		}

	}

	return argc;
}

/***************************************************************

						printError() function
					Prints errors to the console

***************************************************************/

void SerialConsole::printError(int8_t err) {
	//since the table starts at index 0, every error is one unit ahead
	if(err < 0) {
		err = abs(err+1);
		
		//Serial.println(err);
		if(static_cast<uint8_t>(err) >= 0 and static_cast<uint8_t>(err) < ( sizeof(Errors::err_table) / sizeof(Errors::err_table[0]) )) {
			strcpy_P(data, (char *)pgm_read_word(&Errors::err_table[err]));
			Serial.println(data);
		}
	}
}

/***************************************************************

				shiftRight() function
			Shifts the input array one place to the right

***************************************************************/

void SerialConsole::shiftRight(char input[], int8_t input_size) {
	for(int8_t i = input_size; i >= 0; --i) 
		input[i+1] = input[i];
}

/***************************************************************

				shiftLeft() function
			Shifts the input array one place to the left

***************************************************************/

void SerialConsole::shiftLeft(char input[], int8_t input_size) {
	int8_t i;
	for( i = 0; i < input_size; ++i) 
		input[i-1] = input[i];
	input[i-1] = ' ';
}

/***************************************************************

				digitCount() function
					counts digits :)

***************************************************************/

int8_t SerialConsole::digitCount(int8_t num) {

  int8_t cnt = 1;

  while((num/= 10) > 0) ++cnt;
  return cnt;
}

/***************************************************************

				backCursor() function
			moves the cursor x ammount in the same line

***************************************************************/

void SerialConsole::backCursor(int8_t x) {
  uint8_t number_cnt = digitCount(x);

  char temp[4 + number_cnt];
  
  sprintf(temp,"\e[%dD",x);
  Serial.write(temp,4 + number_cnt);
  
}

/***************************************************************

				turnOffCursor() function
			

***************************************************************/

void SerialConsole::turnOffCursor(void) {

  char temp[] = {'\e', '[', '?','2','5','l'};
  
  Serial.write(temp,6);
  
}

/***************************************************************

				turnOnCursor() function
			

***************************************************************/

void SerialConsole::turnOnCursor(void) {

  char temp[] = {'\e', '[', '?','2','5','h'};
  
  Serial.write(temp,6);
  
}

//DEBUG
#ifdef DEBUG
void SerialConsole::printArray(const char *input, const uint8_t rows, const uint8_t columns) {
  uint16_t j = 0;
  for (int i = 0; i < rows; i++) {
    j = 0;
    if (input[j + i * columns] != '\0') {
      for (; j < columns; j++)
        if (input[j + i * columns] != '\0')
          Serial.print(input[j + i * columns]);
      Serial.println();
    }
  }

}

//DEBUG
void SerialConsole::printChars(const char *input, const uint8_t rows, const uint8_t columns) {
  uint16_t j = 0;
  for (int i = 0; i < rows; i++) {
    j = 0;
    if (input[j + i * columns] != '\0') {
      for (; j < columns; j++) {

        if (input[j + i * columns] != '\0' and input[j + i * columns] != '\r' and input[j + i * columns] != '\n') Serial.print(input[j + i * columns]);
        else if(input[j + i * columns] == '\r') Serial.print(" CR");
        else if(input[j + i * columns] == '\n') Serial.print(" NL");
        else Serial.print(" NULL");
        }
      Serial.println();
    } 
    else Serial.println("null");
  }

}

//DEBUG
void SerialConsole::printStringArray(const char **input, const uint8_t size) {
	uint16_t j = 0;
	
	for (int i = 0; i < size; i++) {
		for(int j = 0; input[i][j] != '\0' and j < BUFFER_SIZE; ++j)
			Serial.print(input[i][j]);
		Serial.println();
	}

}
#endif


