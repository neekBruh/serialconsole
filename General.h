//Every define everyone wants to use

#ifndef GENERAL_H
#define GENERAL_H


#define BUFFER_SIZE 64
//#define DEFAULT_CUSTOM_TIMEOUT 70

#define ARGV_ROWS 32 //max individual parameter size allowed

#define DEFAULT_TIMEOUT 7

typedef int8_t(*functionPointer)();

#endif
