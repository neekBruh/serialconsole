#include <SerialConsole.h>

SerialConsole console;

int fun1() {
  Serial.println(F("fun1"));
}

int fun2() {
  Serial.println(F("fun2"));
}

int fun3() {
  Serial.println(F("fun3"));
}

functionPointer functions[] = {&fun1, &fun2, &fun3};

char kw1[] = "fun1";
char kw2[] = "fun2";
char kw3[] = "fun3";

char *keywords[] = {kw1, kw2, kw3}; //esto equivale a un double pointer

void setup() {
  console.init(9600);
  console.attachFunctions(functions, keywords, 3);
  console.showPrompt();
  
}

void loop() {
  console.readSerial();

}
